#!/usr/bin/env python

import random
import subprocess
import sys

from collections import defaultdict

from agalma import config
from agalma import database
from biolite import catalog

random.seed(55761135018)

ids = random.sample(open(sys.argv[1]).readlines(), 20)
ids = '\t'.join(x.rstrip() for x in ids).split()
print "Found ids", ids

sql = """
	SELECT models.catalog_id, sequences.header, sequences.sequence
	FROM agalma_sequences AS sequences JOIN agalma_models AS models
	ON sequences.model_id=models.id
	WHERE models.id=? AND sequences.type=?;"""

data = defaultdict(list)
for id in set(ids):
	for row in database.execute(sql, (id, 'a')):
		data[row[0]].append(">%s\n%s\n" % (row[1].partition(' ')[0], row[2]))
for key in sorted(data):
	print "Writing aa sequences for", key
	open(key+'.pfa', 'w').write(''.join(data[key]))

data = defaultdict(list)
for id in set(ids):
	for row in database.execute(sql, (id, 'n')):
		data[row[0]].append(">%s\n%s\n" % (row[1].partition(' ')[0], row[2]))
for key in sorted(data):
	print "Writing nt sequences for", key
	open(key+'.fa', 'w').write(''.join(data[key]))

for key in sorted(data):
	print "Looking up paths for", key
	paths = catalog.split_paths(catalog.select(key).paths)
	if len(paths) == 2:
		print "Mapping reads for", key
		with open(key+'.log', 'w') as f:
			subprocess.check_call(['bowtie-build', key+'.fa', key],
				stdout=f, stderr=f)
			subprocess.check_call(['bowtie',
				'-q', '--phred33-quals', '-e', '99999999', '-l', '25',
				'-I', '1', '-X', '1200', '-p', '8', '-m', '200',
				key, '-1', paths[0], '-2', paths[1], '-S', '/dev/null',
				'--al', key+'.fq'],
				stdout=f, stderr=f)
	else:
		print "No reads to map for", key

# vim: noexpandtab ts=4 sw=4

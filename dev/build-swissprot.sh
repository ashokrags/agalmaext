#!/bin/bash
#SBATCH -t 30
set -e
DIR=$PWD
cd ~/scratch
wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz
gunzip uniprot_sprot.dat.gz
cd $DIR
python parse-swissprot.py ~/scratch/uniprot_sprot.dat | makeblastdb -dbtype prot -in - -title "Agalma swissprot build $(date)" -out ../agalma/blastdb/swissprot

#!/bin/bash
#SBATCH -t 60
#SBATCH -C intel
#SBATCH --exclusive

set -e

module load openmpi/1.8.3 examl/3.0.14-openmpi raxml/8.2.0

export BIOLITE_TOOLS="examl=examl-AVX"

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -# 100 -m PROTGAMMAWAG -p 12345 -b 12345 -s 13662.fa -n b13662

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -f a -# 100 -m PROTGAMMAWAG -p 12345 -x 12345 -s 13662.fa -n x13662

time /gpfs/runtime/opt/agalma/dev/bin/bl-examl-bootstraps -a 13662.fa -N 100 -m PROTGAMMAWAG -o 13662.newick

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -# 100 -m PROTGAMMAWAG -p 12345 -b 12345 -s 10827.fa -n b10827

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -f a -# 100 -m PROTGAMMAWAG -p 12345 -x 12345 -s 10827.fa -n x10827

time /gpfs/runtime/opt/agalma/dev/bin/bl-examl-bootstraps -a 10827.fa -N 100 -m PROTGAMMAWAG -o 10827.newick

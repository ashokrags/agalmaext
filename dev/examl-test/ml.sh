#!/bin/bash
#SBATCH -t 60
#SBATCH -c 8
#SBATCH -C intel

set -e

module load openmpi/1.8.3 examl/3.0.14-openmpi raxml/8.2.0

export BIOLITE_TOOLS="examl=examl-AVX"

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -m PROTGAMMAWAG -p 12345 -s 13662.fa -n ml13662

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC-PTHREADS -T 8 -m PROTGAMMAWAG -p 12345 -s 13662.fa -n ml813662

time /gpfs/runtime/opt/agalma/dev/bin/bl-examl-bootstraps -a 13662.fa -N 0 -m PROTGAMMAWAG -o ml13662.newick

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -m PROTGAMMAWAG -p 12345 -s 10827.fa -n ml10827

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC-PTHREADS -T 8 -m PROTGAMMAWAG -p 12345 -s 10827.fa -n ml810827

time /gpfs/runtime/opt/agalma/dev/bin/bl-examl-bootstraps -a 10827.fa -N 0 -m PROTGAMMAWAG -o 10827.newick

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC -m PROTGAMMAWAG -p 12345 -s 6846.fa -n ml6846

time /gpfs/runtime/opt/raxml/8.2.0/avx/bin/raxmlHPC-PTHREADS -T 8 -m PROTGAMMAWAG -p 12345 -s 6846.fa -n ml86846

time /gpfs/runtime/opt/agalma/dev/bin/bl-examl-bootstraps -a 6846.fa -N 0 -m PROTGAMMAWAG -o 6846.newick

